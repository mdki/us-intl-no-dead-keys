# us-intl-no-dead-keys

US International keyboard layout without the annoying dead keys. Made with [Microsoft Keyboard Layout Creator (MSKLC)](https://www.microsoft.com/en-us/download/details.aspx?id=102134).

## Easy instructions
Download and run [`build/us-intl2/us-intl2_amd64.msi`](https://gitlab.com/mdki/us-intl-no-dead-keys/-/raw/master/build/us-intl2/us-intl2_amd64.msi?inline=false)

## Build from source
* Open (Ctrl+O) `us-intl-no-dead-keys.klc` with MSKLC
* Optionally go to Project > Properties... and put your own author name and such
* Go to Project > Build DLL and Setup Package